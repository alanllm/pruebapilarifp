<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UF1</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>

<body>
<?php
session_start();

//recogemos los valores de antes, como os he dicho por el atributo y hacemos un $_GET, para recoger los valores
//en caso de ser un form por post, lo haremos con $_POST
$_SESSION['nombre'] = $_GET['nombre'];
$_SESSION['fecha'] = $_GET['fecha'];
$_SESSION['sueldo'] = $_GET['sueldo'];
$_SESSION['ciudad'] = $_GET['cuidad'];

session_destroy();

//ahora aqui os voy a poner un ejemplo de consulta preparada para guardar estos datos en una base de datos.

try {
    //esta es la conexion a nuestra base de datos.
    //en el primer string tiene que ir el conector a la base de datos, en este caso y en el vuestro posiblemente sea mysql
    //luego ponemos el host que es localhost, si trabajamos en local
    //luego ; y le decimos el nombre de la base de datos
    //luego ponemos el usuario de la base de datos
    //y en caso de tener password tambien la ponemos, en caso de no tenerlo lo dejaremos asi
    //$pdo = new PDO("mysql:host=localhost;dbname=prueba", 'usuario');
    //es decir, sin el tercer parametro
    $pdo = new PDO("mysql:host=localhost;dbname=prueba", 'usuario', 'password');
} catch (PDOException $exception) {
    //es importante meter la conexion a la base de datos siempre entre un try catch, ya que es bastante susceptible a errores
    echo $exception->getMessage();
}

//ahora vamos a preparar una query para ejecutarla y guardar los datos en la base de datos
//el metodo prepare es un metodo default que viene con PDO, que es lo que estamos usando para conectarnos
$test = $pdo->prepare("INSERT INTO REGISTRY (nombre, sueldo) VALUES (:nombre, :salario)");
//ahora vamos a bindear los parametros :nombre y :salario, para que sean dinamicos
$test->bindParam(':nombre', $_SESSION['nombre']);
$test->bindParam(':salario', $_SESSION['sueldo']);

//con esto ejecutariamos la query. tambien seria decente meterlo entre un try cach, por si hay algun error, poder manejarlo y saber por que falla
$test->execute();


//Ahora os pongo de ejemplo como seria una sentencia preparada para recoger recursos de la base de datos

//primero preparamos la sentencia que vamos a ejecutar
$sentencia = $pdo->prepare("SELECT * FROM USERS");

//aqui le decimos, que si la sentencia se ha ejecutado bien pasemos al if
if ($sentencia->execute()) {

    //aqui le decimos que mientras haya registros, creemos una variable fila con el contenido que estamos recogiendo de la base de datos
    //es como un loop for o foreach si os suena
    while ($fila = $sentencia->fetch()) {
        //hacemos un print de la fila, que al ser un bucle, hara un print de todos los usuarios de nuestra base de datos
        print_r($fila);
    }
}
?>


<div class="content text-center mt-5">

    <h2>Hola! tu nombre es: <?php echo $_SESSION['nombre'] ?></h2>
    <br>
    <h2>Has nacido: <?php echo $_SESSION['fecha'] ?></h2>
    <br>
    <h2>Tu salario es de: <?php echo $_SESSION['sueldo'] ?></h2>
    <br>
    <h2>Y ahora mismo resides en: <?php echo $_SESSION['ciudad'] ?></h2>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
</body>

</html>