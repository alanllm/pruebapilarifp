<!doctype html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>UF1</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>

  <div class="container">
    <h2 class="text-center">Rellene los datos del formulario</h2>
      <!-- Importante tener en cuenta a donde nos va a llevar este usuario y el metodo que le pones al form
      ya que despues dependera de como recogeras los valores en la siguente pagina.
      El valor lo recogemos en la siguente pagina a traves del atributo name del input-->
    <form action="infousuari.php" method="get">

      <div class="form-group">
        <label for="Nombre">Nombre</label>
        <input type="text" class="form-control" name="nombre">
      </div>

      <div class="form-group">
        <label for="fecha">Fecha de nacimiento</label>
        <input type="date" class="form-control" name="fecha">
      </div>

      <div class="form-group">
        <label for="sueldo">Sueldo</label>
        <input type="text" class="form-control" name=sueldo>
      </div>

      <div class="form-group">
        <label for="cuidad">Cuidad de residencia</label>
        <input type="text" class="form-control" name="cuidad">
      </div>

      <button type="submit" class="btn btn-primary text-right">Enviar</button>
    </form>
  </div>

</body>

</html>